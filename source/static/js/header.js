cloneElements('.inner_items_responsive', '.head_nav_link', 'head_nav_link', 'nav_responsive')
cloneElements('.inner_items_responsive', '.head_btn_contacto', null, 'btn_responsive_contacto')

var openMenu = document.querySelector('.btn_openMenu')
var activated = false

function showResponsive(){
	document.querySelector('.menu_responsive').classList.add('active')
	document.querySelector('.bg_menu').classList.add('active')
	openMenu.classList.add('active')
}

function hidenResponsive(){
	document.querySelector('.menu_responsive').classList.remove('active')
	document.querySelector('.bg_menu').classList.remove('active')
	openMenu.classList.remove('active')
	activated = false


}

openMenu.addEventListener('click', function(e){
	if (activated =! activated) {
		showResponsive()
	} else{
		hidenResponsive()
	}

})
if ( $(window).innerWidth() <= 1024) {
	$('.openSubmenuLink').on('click', function(e){
		e.preventDefault();
		$('.head_nav_link_submenu_responsive').toggleClass('active');
	})
}


$(function(){
	//detectando tablet, celular o ipad
	const isMob2 = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};
	// dispositivo_movil = $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		// tasks to do if it is a Mobile Device
		function readDeviceOrientation() {
			if (Math.abs(window.orientation) === 90) {
				// Landscape
				hidenResponsive()
			} else {
				// Portrait
				hidenResponsive()
			}
		}
		window.onorientationchange = readDeviceOrientation;
	}else{
		$(window).resize(function(){
			var estadomenu = document.querySelector('.menu_responsive').innerWidth;
			if(estadomenu != 0){
				hidenResponsive()
			}
		});
	}
})
