const tabJs = function(fatherNva, navTba, contentTab, contains){
	var tabs = Array.prototype.slice.apply(document.querySelectorAll(navTba));
	var panels = Array.prototype.slice.apply(document.querySelectorAll(contentTab));
	document.getElementById(fatherNva).addEventListener('click', function(e){
		e.preventDefault()
		if(e.target.classList.contains(contains)){
			var i = tabs.indexOf(e.target)
			tabs.map(function(tab){
				tab.classList.remove('active')
			})
			tabs[i].classList.add('active')
			panels.map(function(tab){
				tab.classList.remove('active')
			})
			panels[i].classList.add('active')
		}
	})
}
const Tiempo = function(Element){
	var anio = new Date()
	var mes = anio.getFullYear()
	var htmlMes = document.getElementsByClassName(Element)
	for (var i= 0; i < htmlMes.length; i++){
		htmlMes[i].innerHTML = mes
	}
}

Tiempo('year');


const transitionDelay = function(e, efecto){
	var link = document.querySelectorAll(e);
	for (i = 0; i < link.length;  i++){
		link[i].style.transitionDelay = i/4 + 's'
		link[i].classList.add(efecto)
	}
}
const remove_transitionDelay = function(e, efecto){
	var link = document.querySelectorAll(e);
	for (i = 0; i < link.length;  i++){
		link[i].classList.remove(efecto)
	}
}
const cloneElements = function(father, clonar, removeClass, addElemnt){
	var asideResponsive = document.querySelector(father)
	var menuList = document.querySelector(clonar)
	var cloneElement = menuList.cloneNode(true)
	cloneElement.classList.remove(removeClass)
	cloneElement.classList.add(addElemnt)
	asideResponsive.appendChild(cloneElement);
}

const acordeonJs = function(parametro1, parametro2){
	$(parametro1).click(function(){
		if ($(this).hasClass('is-active')){
			$(this).next(parametro2).stop().slideUp().css('height', 'auto');
			$(this).removeClass('is-active');
		}
		else {
		  $(parametro1).not(this).next(parametro2).stop().slideUp();
		  $(parametro1).not(this).removeClass('is-active');
		  $(this).next(parametro2).stop().slideDown();
		  $(this).addClass('is-active');
		}
	});
}

function changeImage(banners){
   var win = window.innerWidth;
   var elementImg = document.querySelectorAll(banners);
   for(var i = 0; i < elementImg.length; i++){
      var forElement = elementImg[i]
      if (forElement.hasAttribute('data-image')){
         var dataImg = forElement.getAttribute('data-image').split(';');
         if(win > 768){
            forElement.style.backgroundImage = 'url('+dataImg[0]+')'
         }
         if(win <= 768){
            forElement.style.backgroundImage = 'url('+dataImg[1]+')'
         }
         if(win <= 620){
            forElement.style.backgroundImage = 'url('+dataImg[2]+')'
         }
      } else{
         return false
      }
   }
};
changeImage('.mundo');
window.onresize = function(){
	changeImage('.mundo');
};

$(function(){
	// Ancla scroll - AGREGAR CLASE DEL ENLACE
	$('.navScroll').click(function(){
		if(location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')&& location.hostname == this.hostname) {
			var $target = $(this.hash);
			$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
			if ($target.length) {
			var targetOffset = $target.offset().top -80;
			$('html,body').animate({scrollTop: targetOffset}, 1000);
			return false;
			}
		}
	});
	// Reseteando cajas de texto administrables
	$('.no-style *').removeAttr('style');

	// Menu responsive traslucido con scrolling
	document.addEventListener('scroll', function(){
		var scrolling = window.scrollY;
		try {
			if (scrolling > 40) {
				document.querySelector('.head_box_item').classList.add('scroll');
			}else{
				document.querySelector('.head_box_item').classList.remove('scroll');
			};
		} catch (e) {
			console.log(e)
		}
	})
	// SACARLO Y PONERLO DONDE HAY FORMULARIOS
	// $("form").validationEngine('attach', {
	// 	promptPosition : "topLeft",
	// 	autoHidePrompt: true,
	// 	autoHideDelay: 3000,
	// 	binded: false,
	// 	scroll: false
	// });
	// $('btnModal').on('click', function(event) {
	// 	event.preventDefault();
	// 	var valid = $("#modalForm").validationEngine('validate');
	// 	if (!valid) {
	// 		//- return false;
	// 	}
	// 	else{
	// 		//return true;
	// 		$("#modalForm").submit();
	// 	}
	// });
});
